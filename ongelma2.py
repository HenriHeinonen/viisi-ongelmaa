# https://blog.svpino.com/2015/05/07/five-programming-problems-every-software-engineer-should-be-able-to-solve-in-less-than-1-hour
# Problem 2
# Write a function that combines two lists by alternatingly taking elements. For example: given the two lists [a, b, c] and [1, 2, 3], the function should return [a, 1, b, 2, c, 3].

def yhdista_listat(lista1, lista2):
    lista3 = []
    pituus = min( len(lista1), len(lista2) )
    for i in range(0,pituus):
        lista3.append(lista1[i])
        lista3.append(lista2[i])
    return lista3

lista = yhdista_listat(['a', 'b', 'c'], [1, 2, 3])
print lista