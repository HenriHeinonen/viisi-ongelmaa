# -*- coding: utf-8 -*-
# https://blog.svpino.com/2015/05/07/five-programming-problems-every-software-engineer-should-be-able-to-solve-in-less-than-1-hour
# Problem 5
# Write a program that outputs all possibilities to put + or - or nothing between the numbers 1, 2, ..., 9 (in this order) such that the result is always 100. For example: 1 + 2 + 34 - 5 + 67 - 8 + 9 = 100.
def kolmoset(stringi):
    if ',' in stringi:
        plus = stringi.replace(",", "+", 1)
        miinus = stringi.replace(",", "-", 1)
        yhteen = stringi.replace(",", "", 1)
    if ',' in plus:
        kolmoset(plus)
        kolmoset(miinus)
        kolmoset(yhteen)
    else:
        if (eval(plus) == 100):
            print plus + " = ", eval(plus)
            print "----"
        if (eval(miinus) == 100):
            print miinus + " = ", eval(miinus)
            print "----"
        if (eval(yhteen) == 100):
            print yhteen + " = ", eval(yhteen)
            print "----"
    return plus, miinus, yhteen

plus, miinus, yhteen = kolmoset("1,2,3,4,5,6,7,8,9")
