# https://blog.svpino.com/2015/05/07/five-programming-problems-every-software-engineer-should-be-able-to-solve-in-less-than-1-hour
# Problem 1
# Write three functions that compute the sum of the numbers in a given list using a for-loop, a while-loop, and recursion.

def for_funktio():
    summa = 0
    for x in lista:
        summa = summa + x
    print "funktion for_funktio laskema summa = ", summa

def while_funktio():
    summa = 0
    i = 0
    pituus = len(lista)
    while (i < pituus):
        summa = summa + lista[i]
        i = i + 1
    print "funktion while_funktio laskema summa = ", summa

def rekursio(indeksi, summa):
   summa = summa + lista[indeksi]
   if (indeksi+1 < len(lista)):
     rekursio(indeksi+1, summa)
   else:
     print "rekursiivisen funktion laskema summa = ", summa

lista = [1, 2, 3]
print lista
for_funktio()
while_funktio()
rekursio(0,0)