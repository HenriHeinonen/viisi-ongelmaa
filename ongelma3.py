# -*- coding: utf-8 -*-
# https://blog.svpino.com/2015/05/07/five-programming-problems-every-software-engineer-should-be-able-to-solve-in-less-than-1-hour
# Problem 3
# Write a function that computes the list of the first 100 Fibonacci numbers. By definition, the first two numbers in the Fibonacci sequence are 0 and 1, and each subsequent number is the sum of the previous two. As an example, here are the first 10 Fibonnaci numbers: 0, 1, 1, 2, 3, 5, 8, 13, 21, and 34.
# K�ytt�: fibo = laske_fibo(LISTA FIBONACCIN LUKUJONON ENSIMM�ISIST� LUVUISTA, MAKSIMI)
# Esimerkki: fibo = laske_fibo([0, 1, 1, 2], 10)

def laske_fibo(fibo, maksimi):
    alkupituus = len(fibo)
    for i in range(alkupituus, maksimi):
        fibo.append(fibo[i-2] + fibo[i-1])
    return fibo

fibo = laske_fibo([0, 1], 100)
print fibo