# -*- coding: utf-8 -*-
# https://blog.svpino.com/2015/05/07/five-programming-problems-every-software-engineer-should-be-able-to-solve-in-less-than-1-hour
# Problem 4
# Write a function that given a list of non negative integers, arranges them such that they form the largest possible number. For example, given [50, 2, 1, 9], the largest formed number is 95021.
# Käyttö suurin = etsi_suurin(LISTA EI-NEGATIIVISISTA KOKONAISLUVUISTA)
# Esimerkki: suurin = etsi_suurin([50, 2, 1, 9])
# Tulos:
# 50219
# ... (kaikki eri kombinaatiot)
# 91250
# Suurin on  95021

import itertools

def etsi_suurin(lista):
    testi = ""
    testi2 = ""
    pituus = len(lista) # Lasketaan listan koko, jottei sitä tarvitse silmukassa laskea montaa kertaa uudelleen
    for x in range(0, pituus):
        if (x == 0):
            testi = str(lista[x]) # Listan ensimmäisen alkion eteen ei tule pilkkua
        else:
            testi = testi + "," + str(lista[x]) # Listan muiden alkioiden eteen tulee pilkku
    
    laskuri = 0
    maksimi = len(list(itertools.permutations(testi.split(','))))
    for elem in itertools.permutations(testi.split(',')):
        laskuri = laskuri + 1
        if ( laskuri==maksimi ):
            testi2 = testi2 + "".join(elem) # Ei laiteta rivinvaihtoa viimeisen luvun perään
        else:
            testi2 = testi2 + "".join(elem) + "\n" # Laitetaan rivinvaihto muiden lukujen perään
    
    print testi2 # Tulostetaan kaikki kombinaatiot
    
    suurin = 0
    for line in testi2.split('\n'):
        if (int(line) >= suurin):
            suurin = int(line)

    return suurin

suurin = etsi_suurin([50, 2, 1, 9])
print "Suurin on ", suurin

suurin = etsi_suurin([50, 2, 1, 9, 99, 5])
print "Suurin on ", suurin